#! /bin/sh

set -e

apk update
apk upgrade

apk add mysql-client
apk add python3

apk add py3-pip
pip install awscli
apk del py-pip

apk add curl
curl -L --insecure https://github.com/odise/go-cron/releases/download/v0.0.6/go-cron-linux.gz | zcat > /usr/local/bin/go-cron
chmod u+x /usr/local/bin/go-cron
apk del curl

rm -rf /var/cache/apk/*
